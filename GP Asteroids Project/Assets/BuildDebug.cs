﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildDebug : MonoBehaviour
{
    public Text text;
    public static string logs;
    public static BuildDebug stat;

    // Start is called before the first frame update
    void Awake()
    {
        stat = this;
        text.GetComponent<Text>();
    }

    public void UpdateText()
    {
        text.text = logs;
    }

    public static void Log(string message)
    {
        logs += "\n" + message;

        if (stat != null)
            stat.UpdateText();
    }
}
