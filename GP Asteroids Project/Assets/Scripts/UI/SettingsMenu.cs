﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public Text sfxVolumeLabel;
    public Text musicVolumeLabel;

    private bool _isFullscreen;
    public Button fullscreenToggleButton;
    public Sprite onButtonImage;
    public Sprite offButtonImage;

    private int _currentResolutionIndex;
    private List<string> _resolutionLabels = new List<string>();
    private List<Resolution> _resolutions = new List<Resolution>();
    public Text screenResolutionLabel;

    private int _currentColourIndex;
    public Material _shipMaterial;
    public Text shipColourLabel;
    private string[] _colourLabels;
    private Color[] _colours;


    public void Start()
    {
        _currentColourIndex = 0;
        _colourLabels = new string[] {
            "Red",
            "Magenta",
            "Blue",
            "Green",
            "Yellow",
            "Cyan",
            "White",
            "Black"
        };
        _colours = new Color[] {
            Color.red,
            Color.magenta,
            Color.blue,
            Color.green,
            Color.yellow,
            Color.cyan,
            Color.white,
            Color.black
        };

        _currentResolutionIndex = 0;
        _isFullscreen = Screen.fullScreen;
        fullscreenToggleButton.image.sprite = _isFullscreen ? onButtonImage : offButtonImage;
        Resolution[] resolutions = Screen.resolutions;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string resolution = $"{resolutions[i].width}x{resolutions[i].height}";

            if (!_resolutionLabels.Contains(resolution))
            {
                _resolutions.Add(resolutions[i]);
                _resolutionLabels.Add(resolution);

                if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
                {
                    _currentResolutionIndex = i;
                    screenResolutionLabel.text = resolution;
                }
            }
        }

        shipColourLabel.text = _colourLabels[_currentColourIndex];
        _shipMaterial.color = _colours[_currentColourIndex];
    }

    private string _CalculateVolumePercentage(float volume)
    {
        float value = (60 + volume) / 60 * 100;

        return Mathf.RoundToInt(value).ToString();
    }

    public void SetSFXVolume(float volume)
    {
        audioMixer.SetFloat("SFXVolume", volume);

        sfxVolumeLabel.text = _CalculateVolumePercentage(volume);
    }

    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("MusicVolume", volume);

        musicVolumeLabel.text = _CalculateVolumePercentage(volume);
    }

    public void SetFullscreen()
    {
        _isFullscreen = !_isFullscreen;

        fullscreenToggleButton.image.sprite = _isFullscreen ? onButtonImage : offButtonImage;

        Screen.fullScreen = _isFullscreen;
    }

    public void CycleResolutions(int buttonDirection)
    {
        switch (buttonDirection)
        {
            case 0:
                if (_currentResolutionIndex != 0)
                {
                    _currentResolutionIndex -= 1;
                }
                break;
            case 1:
                if (_currentResolutionIndex != (_resolutions.Count - 1))
                {
                    _currentResolutionIndex += 1;
                }
                break;
        }

        screenResolutionLabel.text = _resolutionLabels[_currentResolutionIndex];
        Resolution resolution = _resolutions[_currentResolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, _isFullscreen, 60);
    }

    public void CycleColours(int buttonDirection)
    {
        switch (buttonDirection)
        {
            case 0:
                if (_currentColourIndex != 0)
                {
                    _currentColourIndex -= 1;
                }
                else
                {
                    _currentColourIndex = _colours.Length - 1;
                }
                break;
            case 1:
                if (_currentColourIndex != (_colours.Length - 1))
                {
                    _currentColourIndex += 1;
                }
                else
                {
                    _currentColourIndex = 0;
                }
                break;
        }

        shipColourLabel.text = _colourLabels[_currentColourIndex];
        _shipMaterial.SetColor("_Color", _colours[_currentColourIndex]);
    }
}
