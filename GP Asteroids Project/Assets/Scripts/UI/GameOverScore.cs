﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScore : MonoBehaviour
{
    private int score = 0;
    private int scoreIndex = -1;

    public GameObject nameField;
    public Text scoreText;
    public Text highScoreText;
    public Button saveButton;

    void Start()
    {
        nameField.SetActive(false);
        highScoreText.gameObject.SetActive(false);
        saveButton.gameObject.SetActive(false);

        List<HighScores.HighscoreEntry> scores = HighScores.LoadScores();
        score = PlayerPrefs.GetInt("score");
        
        if (scores != null)
        {
            scores.Sort(HighScores.SortByScore);
            scoreText.text = "Score: " + score.ToString();
        }
        else
        {
            scores = new List<HighScores.HighscoreEntry>();
        }
        
        int count = scores.Count;

        if (count != 0)
        {
            if (count < 5)
            {
                scoreIndex = count;
            }

            for (int i = 0; i < count; i++)
            {
                if (score > scores[i].score)
                {
                    scoreIndex = i;
                }
            }
        }
        else
        {
            scoreIndex = 0;
        }
        
        if (scoreIndex != -1)
        {
            nameField.SetActive(true);
            highScoreText.gameObject.SetActive(true);
            saveButton.gameObject.SetActive(true);
        }
    }

    public void SaveScore()
    {
        string name = nameField.GetComponent<InputField>().text;

        if (name != "")
        {
            HighScores.SaveScore(score, name, scoreIndex);
        }

        nameField.SetActive(false);
        saveButton.gameObject.SetActive(false);
    }

}
