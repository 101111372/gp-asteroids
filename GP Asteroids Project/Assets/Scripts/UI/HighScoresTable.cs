﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoresTable : MonoBehaviour
{
    public List<Text> nameText;
    public List<Text> scoreText;

    void Start()
    {
        List<HighScores.HighscoreEntry> scores = HighScores.LoadScores();

        if (scores != null)
        {
            scores.Sort(HighScores.SortByScore);
            scores.Reverse();

            for (int i = 0; i < scores.Count; i++)
            {
                nameText[i].text = scores[i].name;
                scoreText[i].text = scores[i].score.ToString();
            }
        }
    }
}
