﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HighScores : MonoBehaviour
{
    public static List<HighscoreEntry> LoadScores()
    {
        CheckSaveExists();
        string loadData = PlayerPrefs.GetString("Highscores");
        HighScoreData highscores = JsonUtility.FromJson<HighScoreData>(loadData);
        return highscores.scores;
    }

    public static void SaveScore(int score, string name, int index)
    {
        List<HighscoreEntry> highscores = LoadScores();

        if (highscores == null)
        {
            highscores = new List<HighscoreEntry>();
        }

        HighscoreEntry newScore = new HighscoreEntry{ score = score, name = name };

        if (index > -1 && index < 5 && highscores.Count == 5)
        {
            highscores.RemoveAt(index);
        }

        highscores.Add(newScore);

        HighScoreData data = new HighScoreData
        {
            scores = highscores
        };
        string saveData = JsonUtility.ToJson(data);

        PlayerPrefs.SetString("Highscores", saveData);
        PlayerPrefs.Save();
    }
    public static int SortByScore(HighscoreEntry e1, HighscoreEntry e2)
    {
        return e1.score.CompareTo(e2.score);
    }

    public static void CheckSaveExists()
    {
        if (!PlayerPrefs.HasKey("Highscores") || PlayerPrefs.GetString("Highscores") == null || PlayerPrefs.GetString("Highscores") == "")
        {
            BuildDebug.Log("");
            List<HighscoreEntry> highscores = new List<HighscoreEntry>();
            
            HighscoreEntry newScore = new HighscoreEntry { score = 100, name = "Your Mom" };

            highscores.Add(newScore);

            HighScoreData data = new HighScoreData
            {
                scores = highscores
            };
            string saveData = JsonUtility.ToJson(data);

            PlayerPrefs.SetString("Highscores", saveData);
            PlayerPrefs.Save();
        }
    }

    [Serializable]
    public class HighScoreData
    {
        public List<HighscoreEntry> scores;
    }

    [Serializable]
    public class HighscoreEntry
    {
        public int score;
        public string name;
    }
}
