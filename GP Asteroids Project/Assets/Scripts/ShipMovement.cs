﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{
    public float thrust;
    public float turnSpeed;
    public Rigidbody RB;

    public ParticleSystem backThrustPS;
    public ParticleSystem leftThrustPS;
    public ParticleSystem rightThrustPS;

    public Transform invulnerableEffect;

    public float collisionCooldown = 3;
    private float lastCollision = 0;

    public cameraMode mode = cameraMode.wrap;
    public Vector2 wrapScreenSize;
    public float cameraHeight = 20;

    public Vector3 cameraDrift = Vector3.zero;
    public float maxCameraDrift = 5;

    public Vector3 position
    {
        get { return transform.position; }
    }

    // Start is called before the first frame update
    void Start()
    {
        RB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        RotateAndStabilize();
        ThrustAndCamera();
        ManageInvulnerableEffect();
    }

    private void ManageInvulnerableEffect()
    {
        if (Time.time < lastCollision + collisionCooldown)
        {
            invulnerableEffect.gameObject.SetActive(true);
            float phase = (Time.time - lastCollision) / collisionCooldown;
            Debug.Log("Phase: " + phase);
            float s = Mathf.Min(Mathf.PingPong(phase, 0.5f) * 5, 1);
            Debug.Log("S: " + s);
            invulnerableEffect.localScale = new Vector3(s, s, s);
        }
        else
        {
            invulnerableEffect.gameObject.SetActive(false);
        }
    }

    private void ThrustAndCamera()
    {
        //Add Thrust
        float verticalInput = Input.GetAxis("Vertical");
        Vector3 acceleration = transform.forward * verticalInput * thrust * Time.deltaTime;
        RB.velocity += acceleration;

        if (mode == cameraMode.wrap)
        {
            if (transform.position.x > wrapScreenSize.x)
                transform.position = new Vector3(-wrapScreenSize.x, transform.position.y, transform.position.z);
            if (transform.position.x < -wrapScreenSize.x)
                transform.position = new Vector3(wrapScreenSize.x, transform.position.y, transform.position.z);

            if (transform.position.z > wrapScreenSize.y)
                transform.position = new Vector3(transform.position.x, transform.position.y, -wrapScreenSize.y);
            if (transform.position.z < -wrapScreenSize.y)
                transform.position = new Vector3(transform.position.x, transform.position.y, wrapScreenSize.y);
        }
        else if (mode == cameraMode.follow)
        {
            cameraDrift = Vector3.Lerp(cameraDrift, acceleration.normalized * maxCameraDrift, 0.1f * Time.deltaTime);
            Camera.main.transform.position = transform.position + new Vector3(0, cameraHeight, 0) - cameraDrift;
        }

        float horizontalInput = Input.GetAxis("Horizontal");
        if (horizontalInput != 0)
            horizontalInput = Mathf.Sign(horizontalInput);

        SetThrusterSize(backThrustPS, verticalInput + 1);
        //Left = backwards + turning left
        SetThrusterSize(leftThrustPS, ((verticalInput * -1) + (horizontalInput * -1)) + 1);
        SetThrusterSize(rightThrustPS, ((verticalInput * -1) + horizontalInput) + 1);
    }

    private void SetThrusterSize(ParticleSystem thruster, float value)
    {
        value = Mathf.Clamp(value, 0, 2);

        ParticleSystem.EmissionModule emission = thruster.emission;
        emission.rateOverTime = value * 10;

        ParticleSystem.MainModule main = thruster.main;
        main.startLifetime = value * 0.5f;
    }

    private void RotateAndStabilize()
    {
        //Rotate
        float stabilizeFactor = 3;
        if (Input.GetAxis("Horizontal") > 0)
            transform.Rotate(transform.up * turnSpeed * Time.deltaTime);
        else if (Input.GetAxis("Horizontal") < 0)
            transform.Rotate(transform.up * -turnSpeed * Time.deltaTime);
        else
            stabilizeFactor = 1;

        LimitRotation(stabilizeFactor);
    }

    private void LimitRotation(float stabilizeFactor)
    {
        //Remove angular velocity gradually.
        RB.angularVelocity = Vector3.MoveTowards(RB.angularVelocity, Vector3.zero, Time.deltaTime * stabilizeFactor * 0.2f);

        //Remove non-y-axis rotation gradually.
        Vector3 rotEuler = transform.rotation.eulerAngles;
        rotEuler.x = 0;
        rotEuler.z = 0;
        Quaternion rot = new Quaternion();
        rot.eulerAngles = rotEuler;
        transform.rotation = rot;
    }

    public enum cameraMode
    {
        follow,
        wrap
    }

    private void ActivatePickup(Pickup.Effects effect, Vector3 collisionPoint)
    {
        switch (effect)
        {
            //Repair does nothing in this component.
            case Pickup.Effects.BigLaser:
                
                break;
        }
        GameRules.stat.PickupActivated(effect, collisionPoint);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (Time.time > lastCollision + collisionCooldown)
        {
            Meteor obstacle = collision.transform.GetComponentInParent<Meteor>();
            if (obstacle != null)
            {
                lastCollision = Time.time;
                obstacle.Destroy();
                GameRules.stat.PlayerHit(collision.GetContact(0).point);
            }
            Pickup pickup = collision.transform.GetComponent<Pickup>();
            if (pickup != null)
            {
                ActivatePickup(pickup.effect, collision.GetContact(0).point);
            }
        }
    }
}
