﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisManager : MonoBehaviour
{
    public List<GameObject> possibleDebrisObjectPres = new List<GameObject>();

    private List<GameObject> backgroundObjects = new List<GameObject>();
    private ShipMovement player;

    private Vector3 lastUpdatePos = Vector3.zero;
    private float lastUpdateTime = 0;
    public float timeBetweenUpdates = 5;
    public int maxDebrisCount = 10;
    public int genRate = 1;
    public int startingDebris = 3;

    private Transform debrisHolder;

    public float debRadius
    {
        get { return GameRules.stat.simulationRadius * 15; }
    }

    // Start is called before the first frame update
    void Start()
    {
        debrisHolder = new GameObject("DebrisHolder").transform;
        player = GameRules.stat.player;
        GenerateInitialDebris();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameRules.stat.pause)
            DebrisGeneration();
    }

    private void GenerateInitialDebris()
    {
        for (int i = 0; i < startingDebris; ++i)
        {
            Vector2 pos = Random.insideUnitCircle;
            CreateDebris(new Vector3(pos.x, 0, pos.y) * debRadius * 0.5f);
        }
    }

    private void DebrisGeneration()
    {
        bool doCheck = false;
        if (Vector3.Distance(GameRules.stat.player.position, lastUpdatePos) > debRadius / 2)
            doCheck = true;
        else if (backgroundObjects.Count < 0)
            doCheck = true;
        else if (Time.time > lastUpdateTime + timeBetweenUpdates)
            doCheck = true;

        if (doCheck)
        {
            Vector3 movement = player.position - lastUpdatePos;

            lastUpdatePos = player.position;
            lastUpdateTime = Time.time;

            List<GameObject> toDestroy = new List<GameObject>();

            foreach (GameObject debritus in backgroundObjects)
            {
                Vector3 debPos = debritus.transform.position;
                debPos.y = 0;
                if (Vector3.Distance(debPos, player.position) > debRadius * 1.1f)
                    toDestroy.Add(debritus);
            }

            foreach (GameObject d in toDestroy)
            {
                backgroundObjects.Remove(d);
                Destroy(d);
            }

            int j = 0;
            while (backgroundObjects.Count < maxDebrisCount && j < genRate)
            {
                ++j;
                Vector2 pos = Random.insideUnitCircle.normalized;
                Vector3 biasedPos = movement.normalized + (new Vector3(pos.x, 0, pos.y) * 5);
                biasedPos = biasedPos.normalized * debRadius;
                CreateDebris(biasedPos);
            }
        }
    }

    private void CreateDebris(Vector3 position)
    {
        position.y = -1000;

        Quaternion rot = new Quaternion();
        rot.eulerAngles = new Vector3(0, Random.value * 360, 0);

        GameObject debris = Instantiate(possibleDebrisObjectPres[Random.Range(0, possibleDebrisObjectPres.Count)], player.position + position, rot, debrisHolder);
        
        debris.GetComponent<Rigidbody>().velocity = player.RB.velocity;

        backgroundObjects.Add(debris);
    }
}
