﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerupCharge : MonoBehaviour
{
    public Image BackCircle;
    public Image Circle;
    public Text PowerName;

    void Start()
    {
        //Hide();
    }

    public void Hide()
    {
        BackCircle.gameObject.SetActive(false);
    }

    private void Show()
    {
        BackCircle.gameObject.SetActive(true);
    }

    public void UpdatePower(string powerName)
    {
        Show();
        PowerName.text = powerName;
        UpdateCharge(0);
    }

    public void UpdateCharge(float normalisedValue)
    {
        Circle.fillAmount = 1 - normalisedValue;
    }
}
