﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{

    private float fireTime;
    private static float minExistanceTime = 2;

    // Start is called before the first frame update
    void Awake()
    {
        fireTime = Time.time;
        StartCoroutine(DestroyEventually());
    }

    private IEnumerator DestroyEventually()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Meteor m = collision.transform.GetComponent<Meteor>();
        if (m != null)
        {
            m.Hit();
        }
        if (Time.time < fireTime + minExistanceTime)
        {
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<BoxCollider>());
            Destroy(GetComponent<LineRenderer>());
        }
        else
            Destroy(gameObject);
    }
}
