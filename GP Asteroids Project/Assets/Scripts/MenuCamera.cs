﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCamera : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(new Vector3(0, 1f * Time.deltaTime, 0));
    }
}
