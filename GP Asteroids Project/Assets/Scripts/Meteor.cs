﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    public Rigidbody RB;
    public Size size = Size.Large;

    void Awake()
    {
        if (RB == null)
            RB = GetComponent<Rigidbody>();
    }

    public void Hit()
    {
        GameRules.stat.IncreaseScore(this);
        if (size == Size.Small)
            Destroy();
        else
            Split();
    }

    private void Split()
    {
        GameRules.stat.CreateChildAsteroid(transform.localScale.x / 2, transform.position, RB.velocity);
        GameRules.stat.CreateChildAsteroid(transform.localScale.x / 2, transform.position, RB.velocity);
        Destroy();
    }

    public void Destroy()
    {
        GameRules.stat.DestroyMeteor(this);
        Destroy(gameObject);
    }

    public enum Size {
        Large,
        Medium,
        Small
    }

    public enum Object
    {
        Normal,
        PlanetExpress,
        BeachBall
    }
}
