﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigLaser : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Meteor m = collision.transform.GetComponent<Meteor>();
        if (m != null)
        {
            m.Hit();
        }
    }
}
