﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameRules : MonoBehaviour
{
    /// <summary> A static reference to a single instance of this script (Singleton) so that values can be changed
    /// in game with settings or through the script in inspector, but are still easy to access by other scripts.</summary>
    public static GameRules stat;

    //-----References to Objects-----
    [Header("References")]
    public PowerupCharge powerupUI;
    /// <summary> Script which contains references to and functions for controlling various UI elements. </summary>
    public GameUIController UIController;
    /// <summary> Reference to the ship script of the player, for calculating where obstacles should appear/exist. </summary>
    public ShipMovement player;
    /// <summary> List of asteroid prefabs. Add a prefab twice to increase the chance of spawning. </summary>
    public List<GameObject> asteroidPrefabs = new List<GameObject>();
    /// <summary> List of powerup prefabs. </summary>
    public List<GameObject> powerupPrefabs = new List<GameObject>();
    /// <summary> Prefab for the ship exploding. </summary>
    public GameObject shipExplodingPre;

    public GameObject explosionEffectPre;

    public GameObject assDestructionEffectPre;

    public GameObject pickupEffectPre;


    public string gameOverScene = "TempGameOver";

    public RectTransform pausePanel;

    public List<Image> lifeImages = new List<Image>();

    public Sprite lifeFull;
    public Sprite lifeEmpty;

    //-----Public Variables-----
    [Header("Settings")]
    /// <summary> Current score accrued in this session. </summary>
    public int score;
    /// <summary> Max number of lives the player can have. </summary>
    public int maxLives = 3;
    /// <summary> Number of lives the player has. Should be set to the desired default value. </summary>
    public int lives = 3;
    /// <summary> Radius from the player which asteroids/enemies are simulated at. Objects are destroyed outside this
    /// area, and created just inside the edges of it.</summary>
    public float simulationRadius = 30;
    /// <summary> Asteroids will be spawned regardless of time/distance when the number is less than this. </summary>
    public int minAsteroidCount = 5;
    /// <summary> Asteroids will no longer be spawned when the number is larger than this. </summary>
    public int maxAsteroidCount = 20;
    /// <summary> Number of seconds between checks to add new asteroids or remove old ones. Reset by distance or minimum checks. </summary>
    public float timeBetweenUpdates = 5;
    /// <summary> Number of asteroids which can generate each update. </summary>
    public float genRate = 5;
    /// <summary> The range of random velocity that can be applied to the asteroids. </summary>
    public float asteroidVelVariation = 8;
    /// <summary> The magnitude of velocity towards the player that asteroids spawn with. </summary>
    public float asteroidPlayerBias = 2;
    /// <summary> The range of random velocity that powerups can spawn with. </summary>
    public float powerupVelVariation = 8;
    /// <summary> The magnitude of velocity towards the player that powerups spawn with. </summary>
    public float powerupPlayerBias = 2;
    /// <summary> The time between powerup spawns. </summary>
    public float powerupSpawnRate = 10;

    //-----Public HideInInspector Variables-----
    [HideInInspector()]
    public bool pause = false;

    //-----Temporary Debug Variables-----

    //-----Private/Protected Variables-----
    /// <summary> A list of all active asteroids. </summary>
    private List<Meteor> asteroids = new List<Meteor>();
    /// <summary> Parent for meteors to make the inspector heirarchy cleaner. </summary>
    private Transform meteorHolder;
    /// <summary> Parent for pickups to make the inspector heirarchy cleaner. </summary>
    private Transform pickupHolder;
    /// <summary> The last player position that a check was performed. Checks are done every 1/4 of simulation radius. </summary>
    private Vector3 lastUpdatePos = Vector3.zero;
    /// <summary> The last Time.time value that a check was performed at. Reset by distance or minimum checks. </summary>
    private float lastUpdateTime = 0;
    /// <summary> The last Time.time value that a powerup was spawned at. </summary>
    private float lastPowerupSpawnTime = 0;
    /// <summary> The index of the current life. </summary>
    private int lifeIndex = -1;
    /// <summary> Counter for slowly increasing difficulty. </summary>
    private float difficultyOverTime = 0;

    //-----Properties-----
    public GameObject LargeAsteroidPre
    {
        get { return asteroidPrefabs[0]; }
    }

    public GameObject MediumAsteroidPre
    {
        get { return asteroidPrefabs[1]; }
    }

    public GameObject SmallAsteroidPre
    {
        get { return asteroidPrefabs[2]; }
    }

    void Awake()
    {
        if (stat != null)
            Debug.LogError("Duplicate Game Rules Script.");
        stat = this;

        UIController.SetScore(score);

        meteorHolder = new GameObject("MeteorHolder").transform;
        pickupHolder = new GameObject("PickupHolder").transform;

        UpdateLives();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!pause)
            {
                pause = true;
                pausePanel.gameObject.SetActive(true);
                Time.timeScale = 0;
            }
            else
                Resume();
        }

        if (pause)
            return;

        AsteroidGeneration();
        PowerupGeneration();
    }

    public void Resume()
    {
        pause = false;
        pausePanel.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void PlayerHit(Vector3 point)
    {
        lives--;
        UpdateLives();

        Instantiate(explosionEffectPre, point, new Quaternion()).GetComponent<Rigidbody>().velocity = player.RB.velocity;

        if (lives == 0)
        {
            Rigidbody explosionEffectRB = Instantiate(explosionEffectPre, player.transform.position, player.transform.rotation).GetComponent<Rigidbody>();
            explosionEffectRB.transform.localScale = new Vector3(4, 4, 4);
            Instantiate(shipExplodingPre, player.position, player.transform.rotation).GetComponent<Rigidbody>().velocity = player.RB.velocity;
            Destroy(player.gameObject);
            pause = true;
            StartCoroutine(GameOverEventually());
        }
    }

    private void UpdateLives()
    {
        int i = 0;
        foreach (Image life in lifeImages)
        {
            if (maxLives <= i)
                life.enabled = false;
            else
                life.enabled = true;
            if (lives > i)
                life.sprite = lifeFull;
            else
                life.sprite = lifeEmpty;
            ++i;
        }
    }

    public void PickupActivated(Pickup.Effects effect, Vector3 point)
    {
        Instantiate(pickupEffectPre, point, new Quaternion()).GetComponent<Rigidbody>().velocity = player.RB.velocity;
    }

    public void PlayerRepaired()
    {
        if (lives < maxLives)
            lives += 1;

        UpdateLives();
    }

    private IEnumerator GameOverEventually()
    {
        yield return new WaitForSeconds(3);

        SceneManager.LoadSceneAsync(gameOverScene);
    }

    private void PowerupGeneration()
    {
        if (Time.time > lastPowerupSpawnTime + powerupSpawnRate)
        {
            lastPowerupSpawnTime = Time.time;
            CreatePowerup();
        }
    }
    
    public void CreatePowerup()
    {
        Vector3 randomPoint = Random.insideUnitSphere;
        randomPoint = randomPoint + (player.RB.velocity.normalized * 0.2f);
        randomPoint.y = 0;
        randomPoint.Normalize();

        Pickup powerup = Instantiate(powerupPrefabs[Random.Range(0, powerupPrefabs.Count)], player.position + randomPoint * simulationRadius, Random.rotation, pickupHolder).GetComponent<Pickup>();
        
        Vector3 velVariation = Random.insideUnitSphere;
        Vector3 playerBias = (player.position - powerup.transform.position).normalized;
        velVariation.y = 0;
        powerup.RB.velocity = player.RB.velocity + playerBias * powerupPlayerBias + velVariation * powerupVelVariation;
    }

    //----------------ASTEROID GENERATION----------------//
    private void AsteroidGeneration()
    {
        bool doCheck = false;
        if (Vector3.Distance(player.position, lastUpdatePos) > (simulationRadius * 2))
            doCheck = true;
        else if (asteroids.Count < minAsteroidCount)
            doCheck = true;
        else if (Time.time > lastUpdateTime + timeBetweenUpdates)
            doCheck = true;

        if (doCheck)
        {
            lastUpdatePos = player.position;
            lastUpdateTime = Time.time;

            List<Meteor> toDestroy = new List<Meteor>();

            foreach (Meteor m in asteroids)
            {
                if (Vector3.Distance(m.transform.position, player.position) > simulationRadius * 1.1f)
                    toDestroy.Add(m);
            }

            foreach (Meteor m in toDestroy)
            {
                Unsubscribe(m);
                Destroy(m.gameObject);
            }

            int j = 0;
            while (asteroids.Count < maxAsteroidCount + difficultyOverTime && j < genRate)
            {
                ++j;
                CreateAsteroid(4);
            }
        }
    }

    public void CreateAsteroid(float maxSize)
    {
        Vector3 randomPoint = Random.insideUnitSphere;
        randomPoint += (player.RB.velocity.normalized * 0.2f);
        randomPoint.y = 0;
        randomPoint.Normalize();

        Meteor meteor = Instantiate(asteroidPrefabs[Random.Range(0, asteroidPrefabs.Count)], player.position + randomPoint * simulationRadius, Random.rotation, meteorHolder).GetComponent<Meteor>();
        float size = Random.Range(1f, maxSize);

        if (size > 2.5)
            meteor.size = Meteor.Size.Large;
        else if (size > 1.5)
            meteor.size = Meteor.Size.Medium;
        else
            meteor.size = Meteor.Size.Small;

        meteor.transform.localScale = new Vector3(size, size, size);
        Vector3 velVariation = Random.insideUnitSphere;
        Vector3 playerBias = (player.position - meteor.transform.position).normalized;
        velVariation.y = 0;
        meteor.RB.velocity = player.RB.velocity + playerBias * (asteroidPlayerBias + difficultyOverTime) + velVariation * asteroidVelVariation;
        meteor.RB.angularVelocity = velVariation;
        asteroids.Add(meteor);
    }

    public void CreateChildAsteroid(float maxSize, Vector3 position, Vector3 velocity)
    {
        if (pause)
            return;
        Vector3 randomPoint = Random.insideUnitSphere;
        randomPoint.y = 0;
        randomPoint.Normalize();

        Meteor meteor = Instantiate(asteroidPrefabs[Random.Range(0, asteroidPrefabs.Count)], position + randomPoint * maxSize, Random.rotation, meteorHolder).GetComponent<Meteor>();
        float size = Random.Range(1f, maxSize);

        if (size > 2.5)
            meteor.size = Meteor.Size.Large;
        else if (size > 1.5)
            meteor.size = Meteor.Size.Medium;
        else
            meteor.size = Meteor.Size.Small;

        meteor.transform.localScale = new Vector3(size, size, size);
        Vector3 velVariation = Random.insideUnitSphere;
        velVariation.y = 0;
        meteor.RB.velocity = ((player.RB.velocity * 2 + velocity) / 3) + velVariation;
        meteor.RB.angularVelocity = velVariation;
        asteroids.Add(meteor);
    }

    public void Subscribe(Meteor target)
    {
        asteroids.Add(target);
    }

    public void DestroyMeteor(Meteor target)
    {
        GameObject effect = Instantiate(assDestructionEffectPre, target.transform.position, new Quaternion());
        effect.GetComponent<Rigidbody>().velocity = target.RB.velocity;
        effect.transform.localScale = target.transform.localScale / 2;
        PitchChanger PC = effect.GetComponent<PitchChanger>();
        if (target.size == Meteor.Size.Large)
            PC.specificPitch = 2.2f;
        else if (target.size == Meteor.Size.Medium)
            PC.specificPitch = 2.5f;
        else
            PC.specificPitch = 2.8f;

        Unsubscribe(target);
    }

    public void Unsubscribe(Meteor target)
    {
        asteroids.Remove(target);
        difficultyOverTime += 0.01f;
    }

    public void IncreaseScore(Meteor target)
    {
        if (target.size == Meteor.Size.Large)
            score += 100;
        else if (target.size == Meteor.Size.Medium)
            score += 200;
        else
            score += 300;

        UIController.SetScore(score);
    }
    void OnDisable()
    {
        PlayerPrefs.SetInt("score", score);
    }
}
