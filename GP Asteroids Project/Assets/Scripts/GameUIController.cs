﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{
    public Text scoreText;
    public Text livesText;

    public void SetScore(int score)
    {
        scoreText.text = "SCORE: " + score;
        scoreText.text = score.ToString();
    }

    public void SetLives(int lives)
    {
        livesText.text = "LIVES: " + lives;
    }
}
