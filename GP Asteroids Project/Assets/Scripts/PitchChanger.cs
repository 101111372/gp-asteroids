﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchChanger : MonoBehaviour
{
    public float specificPitch = -1;
    public bool randomize = true;
    public float range = 0.2f;
    public AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        if (source == null)
            source = GetComponent<AudioSource>();
        if (specificPitch != -1)
            source.pitch = specificPitch;
        RandomizePitch();
    }

    public void RandomizePitch()
    {
        float value = range * 0.5f;
        value = Random.Range(-value, value);
        value = source.pitch + value;
        source.pitch = value;
    }
}
