﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectDestroyer : MonoBehaviour
{
    void Awake()
    {
        StartCoroutine(DestroyEventually());
    }

    private IEnumerator DestroyEventually()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
}
