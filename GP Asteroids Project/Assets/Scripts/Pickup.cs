﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Effects effect = Effects.Repair;
    private static float duration = 20;
    private static float flashDuration = 5;
    public Rigidbody RB;
    public Light pointLight;
    public ParticleSystem particleSystem;
    private float pointLightIntensity;

    void Awake()
    {
        pointLightIntensity = pointLight.intensity;
        StartCoroutine(FlashEventually());
    }

    private IEnumerator FlashEventually()
    {
        yield return new WaitForSeconds(duration - flashDuration);
        bool down = true;
        for (int i = 0; i < 10; ++i)
        {
            for (int j = 0; j < 10; ++j)
            {
                if (down)
                {
                    pointLight.intensity -= pointLightIntensity / 10;
                    particleSystem.transform.localScale -= Vector3.one * 0.02f;
                }
                else
                {
                    pointLight.intensity += pointLightIntensity / 10;
                    particleSystem.transform.localScale += Vector3.one * 0.02f;
                }
                yield return new WaitForSeconds(flashDuration / 100);
            }
            down = !down;
        }
        StartCoroutine(ShrinkDestroy());
    }

    private IEnumerator ShrinkDestroy()
    {
        Vector3 size = transform.localScale;
        for (int i = 0; i < 100; ++i)
        {
            transform.localScale -= size / 100;
            particleSystem.transform.localScale -= Vector3.one * 0.002f;
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
    }

    public enum Effects {
        Repair,
        BigLaser,
        RapidFire,
        Shotgun,
        Missile,
        Shield
    }
}
