﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipFiring : MonoBehaviour
{
    private Rigidbody RB;

    public Transform firingPoint;
    public Transform leftFiringPoint;
    public Transform rightFiringPoint;

    public FiringMode mode = default;

    public GameObject laserPre;
    public GameObject missilePre;
    public GameObject bigOlLaserObject;

    public float defaultCooldown = 1;
    public float rapidfireCooldown = 0.1f;
    public float laserVelocity = 20;
    public float missileCooldown = 3;
    public float missileVelocity = 5;
    public float shotgunCooldown = 0.5f;
    public float missilesMaxUses = 10;
    public float rapidFireMaxUses = 100;
    public float shotgunMaxUses = 20;
    public float bigOlLaserMaxUseage = 5;
    private float powerupUseage = 0;

    private float lastShot = -5;


    // Start is called before the first frame update
    void Start()
    {
        RB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameRules.stat.pause)
            return;

        if (Input.GetButton("Fire1"))
        {
            if (mode == FiringMode.Default && Time.time > lastShot + defaultCooldown)
            {
                FireLaser(firingPoint.position, transform.rotation);
                lastShot = Time.time;
            }
            else if (mode == FiringMode.RapidFire && Time.time > lastShot + rapidfireCooldown)
            {

                if (powerupUseage <= rapidFireMaxUses)
                {
                    powerupUseage += 1;
                    FireLaser(firingPoint.position, transform.rotation);
                    lastShot = Time.time;
                    GameRules.stat.powerupUI.UpdateCharge(powerupUseage / rapidFireMaxUses);
                }
                else
                    ResetMode();
            }
            else if (mode == FiringMode.Missile && Time.time > lastShot + missileCooldown)
            {

                if (powerupUseage <= missilesMaxUses)
                {
                    powerupUseage += 1;
                    Rigidbody missileRB = Instantiate(missilePre, firingPoint.position, transform.rotation).GetComponent<Rigidbody>();

                    missileRB.velocity = RB.velocity + (transform.forward * missileVelocity);
                    lastShot = Time.time;
                    GameRules.stat.powerupUI.UpdateCharge(powerupUseage / missilesMaxUses);
                }
                else
                    ResetMode();
            }
            else if (mode == FiringMode.Shotgun && Time.time > lastShot + shotgunCooldown)
            {
                if (powerupUseage <= shotgunMaxUses)
                {
                    powerupUseage += 1;
                    lastShot = Time.time;

                    Vector3 angleDiff = new Vector3(0, 15, 0);
                    Vector3 smallDiff = new Vector3(0, 5, 0);
                    Quaternion rot = transform.rotation;

                    FireLaser(firingPoint.position, rot);

                    rot.eulerAngles = rot.eulerAngles - angleDiff;
                    FireLaser(leftFiringPoint.position, rot);
                    rot.eulerAngles = rot.eulerAngles + smallDiff;
                    FireLaser(leftFiringPoint.position, rot);
                    FireLaser(firingPoint.position, rot);

                    rot.eulerAngles = rot.eulerAngles + angleDiff + smallDiff;
                    FireLaser(firingPoint.position, rot);
                    FireLaser(rightFiringPoint.position, rot);
                    rot.eulerAngles = rot.eulerAngles + smallDiff;
                    FireLaser(rightFiringPoint.position, rot);
                    GameRules.stat.powerupUI.UpdateCharge(powerupUseage / shotgunMaxUses);
                }
                else
                    ResetMode();
            }

            if (mode == FiringMode.BigOlLaser)
            {
                if (powerupUseage <= bigOlLaserMaxUseage)
                {
                    powerupUseage += Time.deltaTime;
                    bigOlLaserObject.SetActive(true);
                    GameRules.stat.powerupUI.UpdateCharge(powerupUseage / bigOlLaserMaxUseage);
                }
                else
                    ResetMode();
            }
            else
                bigOlLaserObject.SetActive(false);

        }
        else
            bigOlLaserObject.SetActive(false);
    }

    private void ResetMode()
    {
        FireLaser(firingPoint.position, transform.rotation);
        mode = FiringMode.Default;
        lastShot = Time.time;
        powerupUseage = 0;
        GameRules.stat.powerupUI.Hide();
    }

    private void FireLaser(Vector3 position, Quaternion rotation)
    {
        Rigidbody laserRB = Instantiate(laserPre, position, rotation).GetComponent<Rigidbody>();

        laserRB.velocity = RB.velocity + (laserRB.transform.forward * laserVelocity);
        lastShot = Time.time;
    }

    public enum FiringMode
    {
        Default,
        RapidFire,
        Missile,
        Shotgun,
        BigOlLaser
    }

    private void ActivatePickup(Pickup.Effects effect, Vector3 collisionPoint)
    {
        switch (effect)
        {
            //Repair does nothing in this component.
            case Pickup.Effects.Repair:
                GameRules.stat.PlayerRepaired();
                break;
            case Pickup.Effects.BigLaser:
                mode = FiringMode.BigOlLaser;
                GameRules.stat.powerupUI.UpdatePower("Big Ol Laser");
                GameRules.stat.powerupUI.UpdateCharge(0);
                break;
            case Pickup.Effects.Missile:
                mode = FiringMode.Missile;
                GameRules.stat.powerupUI.UpdatePower("Missiles");
                GameRules.stat.powerupUI.UpdateCharge(0);
                break;
            case Pickup.Effects.RapidFire:
                mode = FiringMode.RapidFire;
                GameRules.stat.powerupUI.UpdatePower("Machine Gun");
                GameRules.stat.powerupUI.UpdateCharge(0);
                break;
            case Pickup.Effects.Shotgun:
                mode = FiringMode.Shotgun;
                GameRules.stat.powerupUI.UpdatePower("Shotgun");
                GameRules.stat.powerupUI.UpdateCharge(0);
                break;
        }
        powerupUseage = 0;
        GameRules.stat.PickupActivated(effect, collisionPoint);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Pickup pickup = collision.transform.GetComponent<Pickup>();
        if (pickup != null)
        {
            ActivatePickup(pickup.effect, collision.GetContact(0).point);
            Destroy(pickup.gameObject);
        }
    }
}
