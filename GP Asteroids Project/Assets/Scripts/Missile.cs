﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public Rigidbody RB;

    public GameObject explosionPre;
    public GameObject explosionEffectPre;

    public float acceleration = 1;

    public float explosionTimer;


    // Start is called before the first frame update
    void Awake()
    {
        RB = GetComponent<Rigidbody>();
        StartCoroutine(ExplodeEventually());
    }

    void Update()
    {
        RB.velocity += transform.forward * acceleration * Time.deltaTime;
    }

    private IEnumerator ExplodeEventually()
    {
        yield return new WaitForSeconds(explosionTimer);
        Explode();
    }

    private void Explode()
    {
        Rigidbody explosionRB = Instantiate(explosionPre, transform.position, transform.rotation).GetComponent<Rigidbody>();
        explosionRB.velocity = RB.velocity;

        Rigidbody explosionEffectRB = Instantiate(explosionEffectPre, transform.position, transform.rotation).GetComponent<Rigidbody>();
        explosionEffectRB.transform.localScale = new Vector3(4, 4, 4);
        explosionEffectRB.velocity = RB.velocity;

        explosionEffectRB = Instantiate(explosionEffectPre, transform.position + new Vector3(0, 1, 0), Random.rotation).GetComponent<Rigidbody>();
        explosionEffectRB.transform.localScale = new Vector3(3, 3, 3);
        explosionEffectRB.velocity = RB.velocity;

        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Meteor m = collision.transform.GetComponent<Meteor>();
        if (m != null)
        {
            m.Hit();
        }
        Explode();
    }
}
