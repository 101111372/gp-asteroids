﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public float maxSize = 5;
    public float explosionSpeed = 1;
    public float holdMaxTime = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float oldVal = transform.localScale.x;
        if (oldVal >= maxSize)
        {
            holdMaxTime -= Time.deltaTime;
            if (holdMaxTime <= 0)
                Destroy(gameObject);
        }
        else
        {
            float newVal = oldVal + explosionSpeed * Time.deltaTime;
            transform.localScale = new Vector3(newVal, newVal, newVal);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        Meteor m = collision.transform.GetComponent<Meteor>();
        if (m != null)
        {
            m.Hit();
        }
    }
}
