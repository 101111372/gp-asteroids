
Team name: The Big D's

---TEAM---
Jake Scott (101111372): Programmer - Gameplay
Nathan Templar (101631304): Programmer - UI
Patrick Spolidoro (102573737): Programmer - Effects
Dennys Reeb (101974546): Artist - 3D


---CONTROLS---
* Use Mouse to interact with menu/UI

* W/S to accelerate/reverse
* A/D to rotate

* spacebar to fire any weapon

* ESC to pause

* Controller support is implemented but untested


---TOOLS---
Unity
Discord
Slack
Photoshop
Paint.net
Blender
Audacity
BitBucket
SourceTree
GithubDesktop

---RESOURCES---
Starfield Skybox
FreeSounds.org (multiple)
